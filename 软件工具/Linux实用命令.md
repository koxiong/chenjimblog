# Linux实用命令


>记录个人常用配置

-----

## 终端使用
- 禁用Vim退出后回显或者清屏  
在 `~/.bashrc` 添加  
`export TERM=xterm`  


- 命令行终端忽略文件路径tab忽略大小写  
在 ` ~/.inputrc` 添加  
`set completion-ignore-case on`  


- 终端命令单独另起一行  
修改  `~/.bashrc` 中 `PS1` 字段  
在 `\w` 后加 `\n`  

---

## Git Bash 使用

- adb push 注意多一个右斜线  
  `adb push /d/abc/o.apk //system/app/Mine/mine.apk`  

- cd 到 windows 路径需要加引号  
  `cd 'D:\code\Abc'` 或者 `cd "D:\code\Abc"`  

- 增加 alias 如 gl  
  在文件 `C:\Program Files\Git\etc\profile.d\aliases.sh` 增加  
  `alias gl=git log --oneline --all --graph --decorate `  
  `alias e.=explorer .`

- 增加 History 最大保存数
  默认500，可以在 `C:\Program Files\Git\etc\profile.d\aliases.sh` 添加如下以修改
  `HISTSIZE=3000`
  `HISTFILESIZE=3000`

-----

相关文章  
- Git配置和常用命令  
  <https://blog.csdn.net/CSqingchen/article/details/105674924>
- 安卓软件开发常用命令集合  
  <https://blog.csdn.net/CSqingchen/article/details/108150595>  

