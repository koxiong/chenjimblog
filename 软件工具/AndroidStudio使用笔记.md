# AndroidStudio 使用笔记

> 最新更新地址 <https://gitee.com/chenjim/chenjimblog>  

## Markdown Not Support JCEF
1. 问题  
Your environment does not support JCEF,can not use Mardkdown Editor Preview  
Android Studio 默认运行时暂不支持JCEF，无法使用markdown预览  
2. 解决办法  
  方法1：默认运行时路径在 `android-studio\jbr` , 可以备份后，替换为带`JCEF`的运行时  
  方法2：安装新的运行时： `Ctrl + N`，选择 `Actions` , 搜索 `runtime`  
  选择 `Choose Boot Java Runtime for the IDE`, 选择包含`JCEF`的 Runtime  
  确认后，会自动下载 `jbr`，默认保存在目录 `~/.jbr/`  
  ![](https://pic.chenjim.com/202308021552566.png-blog)  
  当 AS 更新后，可能需要下载新的 `jbr` 适配  

---

## Manage IDE Settings
1. 背景  
  通过这个工具菜单，可以将 IDEA 的设置(如快捷键等)同步到 Git 仓库，实现在不同电脑使用相同的设置  
  ![](https://pic.chenjim.com/202308021546764.png-blog)   
  ![](https://pic.chenjim.com/202308021547639.png-blog)  

2. 问题  
  更新 Android Studio 到  2022.3.1，移除了插件 `Settings Repository`，替换为了 `Settings Sync`  
  导致无法正常使用 `Manage IDE Settings`, 自动同步 IDEA 的配置  

3. 解决办法  
  禁用 `Settings Sync` ，重新安装插件 `Settings Repository`  
  也可尝试 [下载 **新版** `Settings Repository`](https://plugins.jetbrains.com/plugin/7566-settings-repository/versions)，使用 IDEA 账户同步    
  当前 2022.3.1 版暂无法使用 `Settings Repository`  

---

## 正使用的好用插件  
- Alibaba Java Coding Guidelines  
  代码格式异常提示  
- Android Drawable Preview  
  预览 Drawable xml 文件  
- Rainbow Brackets  
  彩虹括号 
- MultiHighlight  
  高亮代码 
- Translation  
  翻译插件 
