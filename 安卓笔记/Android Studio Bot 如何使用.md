
# Android Studio Bot 下载使用

> 本文最新更新地址 <https://gitee.com/chenjim/chenjimblog>

- 下载 [Android Studio Hedgehog](https://developer.android.com/studio/archive)  
  当前的新版本是 ` 2023.1.1 Canary 10 `
- 找到`Studio Bot`: View->Tool Windows->Studio Bot，或者下图 
  ![](https://pic.chenjim.com/202306301837655.png-blog)
- 登录 Google 账号，注意当前限制只能US的账户使用 ！！  
  ![](https://pic.chenjim.com/202306301839876.png-blog)
  查看自己 Google 账户服务地区 <https://policies.google.com/terms>  
  如果没有 US 账户，可以尝试开启`全局 Proxy`重新注册账户  
- 注意配置 `Android Studio` 的 Proxy，参考如下  
  ![](https://pic.chenjim.com/202306301842970.png-blog)
- 完成以上步骤后，我们就可以开始我们的 Studio Bot 体验之旅了
  ![](https://img-blog.csdnimg.cn/1522d96040714a928b125231d4b74f6a.gif)


参考文章  
<https://blog.csdn.net/dai_jiawei/article/details/130702636>   
<https://developer.android.com/studio/preview/studio-bot>  