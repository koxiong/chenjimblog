### 博客文章存储

- 本仓库内容来自工作、学习过程中的笔记、摘录，收集便于检索，如有侵权联系删除  
  
- 部分发布在 [清霜之辰_简书](https://www.jianshu.com/u/4f94142e4275) 和  [清霜之辰_CSDN博客](https://blog.csdn.net/CSqingchen)  
  
- [Gitee仓库](https://gitee.com/chenjim/chenjimblog) 与 [Github仓库](https://github.com/chenjim/chenjimblog) 自动同步

- 过去的、错误的就让他过去，**commit历史会被强制覆盖**

- 原创文章，转载请注明出处、原文链接，个人邮件 <me@h89.cn> ，主页 [https://chenjim.com](https://h89.cn)

----

以下内容摘自原文 <https://github.com/getActivity/AndroidIndex>

#### Android 笔记及面试题

* [AndroidInterview-Q-A](https://github.com/JackyAndroid/AndroidInterview-Q-A)

* [CharonChui/AndroidNote](https://github.com/CharonChui/AndroidNote)

* [linsir6/AndroidNote](https://github.com/linsir6/AndroidNote)

* [Awesome-Android-Interview](https://github.com/JsonChao/Awesome-Android-Interview)

* [Android-Daily-Interview](https://github.com/Moosphan/Android-Daily-Interview)

* [AndroidOfferKiller](https://github.com/Blankj/AndroidOfferKiller)

* [Android_QA](https://github.com/Omooo/Android_QA)

* [AndroidBookmark](https://github.com/mikaelzero/AndroidBookmark)

* [Android-Review](https://github.com/JasonWu1111/Android-Review)

* [AndroidTips](https://github.com/johncaijing/AndroidTips)

#### Android 优秀个人技术博客

* [红橙 Darren](https://www.jianshu.com/u/35083fcb7747)

* [weishu](https://weishu.me/)

* [Gityuan](http://gityuan.com/)

* [刘一刀](https://juejin.cn/user/272334612601559/posts)

* [彭旭锐](https://juejin.cn/user/1063982987230392/posts)

* [Flywith24](https://juejin.cn/user/219558054476792/posts)

* [胡飞洋](https://juejin.cn/user/2295436011910894/posts)

* [蓝师傅](https://juejin.cn/user/3298190612500696/posts)

* [业志陈](https://juejin.cn/user/923245496518439/posts)

* [路遥在路上](https://juejin.cn/user/2137106333043070/posts)

* [开发者如是说](https://juejin.cn/user/3685218704691469/posts)

* [依然范特稀西](https://juejin.cn/user/1204720443862887/posts)

* [JessYan](https://juejin.cn/user/976022014539326/posts)

* [恋猫de小郭](https://juejin.cn/user/817692379985752/posts)

* [拉丁吴](https://juejin.cn/user/976022014531134/posts)

* [天之界线2010](https://juejin.cn/user/4336129589120302/posts)

* [究极逮虾户](https://juejin.cn/user/4265760848090664/posts)

* [BaronTalk](https://juejin.cn/user/923245496502232/posts)

* [bennyhuo](https://juejin.cn/user/1187128286120631/posts)

* [yechaoa](https://juejin.cn/user/659362706101735/posts)

* [郭霖](https://blog.csdn.net/guolin_blog)

* [Mlx](https://juejin.cn/user/1345457961046055/posts)

* [貌似许亚军](https://juejin.cn/user/1433418891789149/posts)

* [D_clock爱吃葱花](https://juejin.cn/user/1926000099735373/posts)

* [NanBox](https://juejin.cn/user/3175045309414935/posts)

* [朱涛的自习室](https://juejin.cn/user/2119514149637032/posts)

* [Blankj](https://juejin.cn/user/1978776659167981/posts)

* [Carson 带你学 Android](https://juejin.cn/user/2524134385917293/posts)

* [程序员DHL](https://juejin.cn/user/2594503168898744/posts)

* [秦子帅](https://juejin.cn/user/4089838984240680/posts)

* [吴小龙同學](https://juejin.cn/user/3614849960247351/posts)

* [nanchen2251](https://juejin.cn/user/4230576473377751/posts)

* [唐子玄](https://juejin.cn/user/3087084378135613/posts)

* [北斗星_And](https://juejin.cn/user/3966693684811662/posts)

* [却把清梅嗅](https://juejin.cn/user/1503787635450584/posts)

* [KunMinX](https://juejin.cn/user/1081575170900958/posts)

* [全世界_Coder](https://juejin.cn/user/1890815727121005/posts)

* [刘望舒](https://juejin.cn/user/3016715635792974/posts)

* [xuyisheng](https://juejin.cn/user/43636194286093/posts)

* [codelang](https://juejin.cn/user/184373682638727/posts)

* [赌一包辣条](https://juejin.cn/user/2735240659359448/posts)

* [欧少泽](https://juejin.cn/user/4353721773341934/posts)

* [Android轮子哥](https://www.jianshu.com/u/f7bb67d86765)

#### Github 相关

* [Github Android 国内个人技术排行榜](https://github.com/getActivity/AndroidGithubBoss)

* [GitHub 中文排行榜](https://github.com/GrowingGit/GitHub-Chinese-Top-Charts)

* [Github 程序员全球排行榜](https://wangchujiang.com/github-rank/index.html)

* [中国开源码力榜](https://opensource.win/)

* [Github 趋势库](https://github.com/trending)

* [Github Star 趋势图](https://starchart.cc/)

* [Github User PK](https://githubpk.com/pk-users)

* [Github Repository PK](https://githubpk.com/pk-repos)

* [Studio 精品插件推荐](https://github.com/getActivity/StudioPlugins)

----