
@[toc]
# 安卓mediasoup输出H264流(支持H264编码)


>本文首发地址 <https://blog.csdn.net/CSqingchen/article/details/120218832>  
>最新更新地址 <https://gitee.com/chenjim/chenjimblog>  
>首先得让mediasoup支持H264编解码，参见 [前文](https://blog.csdn.net/CSqingchen/article/details/120199702)  

#### 默认视频编码是VP8源码分析  

相关源码流程、注释如下  
``` java
//文件 RoomClient.java 中 
@WorkerThread
private void joinImpl() {
    mMediasoupDevice = new Device();
    //从服务端获取编解码能力
    String routerRtpCapabilities = mProtoo.syncRequest("getRouterRtpCapabilities");
    //最终会调用到 libmediasoupclient/src/Device.cpp 中  Device::Load 
    mMediasoupDevice.load(routerRtpCapabilities);
}

//文件 libmediasoupclient/src/Device.cpp 中 
void Device::Load(json routerRtpCapabilities, const PeerConnection::Options* peerConnectionOptions){
  ...
  //将设备编码能力和服务端编码能力匹配
  this->extendedRtpCapabilities = ortc::getExtendedRtpCapabilities(nativeRtpCapabilities, routerRtpCapabilities);
  ...
}

//最终视频编码方案代码在  文件 libmediasoupclient/src/Transport.cpp 中 
SendTransport::SendTransport(...){
    ...
    auto sendingRtpParametersByKindV= ortc::getSendingRtpParameters("video", *extendedRtpCapabilities);
}

//查看 libmediasoupclient/src/ortc.cpp 中 getSendingRtpParameters 我们可以看到如下
json getSendingRtpParameters(const std::string& kind, const json& extendedRtpCapabilities){
  ...
  for (const auto& extendedCodec : extendedRtpCapabilities["codecs"]){
    ...
    //找到一个编码器，就 break，跳出了
    // NOTE: We assume a single media codec plus an optional RTX codec.
		break;
  }
  ...
}
```

#### 修改支持H264编码
通过上节分析，我们需要使 `routerRtpCapabilities` 中 `"mimeType": "video/H264",` 字段靠前  
可以在 `RoomClient.java` 中修改，也可以在 `Device.cpp` 中 `Device::Load ` 修改  
以下是用后者的修改方案，**只保留H264**，参考自 [mediasoup支持h264](https://blog.csdn.net/thehunters/article/details/117472011)  
``` diff
--- a/src/Device.cpp
+++ b/src/Device.cpp
@@ -55,9 +55,25 @@ namespace mediasoupclient
      if (this->loaded)
              MSC_THROW_INVALID_STATE_ERROR("already loaded");

+     auto &remoteCaps = routerRtpCapabilities["codecs"];
+     std::string mimeTypeH264 = "video/H264";
+     std::transform(mimeTypeH264.begin(), mimeTypeH264.end(), mimeTypeH264.begin(), ::tolower);
+     for (nlohmann::json::iterator itr=remoteCaps.begin(); itr!= remoteCaps.end();) {
+         nlohmann::json tmp = *itr;
+         std::string tmpCodec = tmp["mimeType"];
+         std::transform(tmpCodec.begin(), tmpCodec.end(), tmpCodec.begin(), ::tolower);
+         if (tmpCodec != mimeTypeH264 && tmp["kind"] == "video") {
+             itr = remoteCaps.erase(itr);
+         } else {
+             itr++;
+         }
+     }
+
      // This may throw.
      ortc::validateRtpCapabilities(routerRtpCapabilities);


```


到这里，SDP已经支持H264编码，最终还得依赖设备的H264编码能力，参考 [前文](https://blog.csdn.net/CSqingchen/article/details/120199702)   


我们可以在 <https://v3demo.mediasoup.org> 看到设备推出流的编码信息，如下图  
![](https://pic.chenjim.com/20210908101841.png-blog)  

---

- 近期看到一份少入侵的修改方式，  
  即依据`HardwareVideoEncoderFactory`创建自定义编解码器 `CustomHardwareVideoEncoderFactory`，  
  具体实现可以参见  <https://github.com/shenbengit/WebRTCExtension>   
  结果会存在实际设备并不支持的情况  


----
其它相关文档  
- 安卓webrtc在ubuntu 2004下编译使用  
<https://blog.csdn.net/CSqingchen/article/details/120016697>

- Android 断点调试 webrtc、 medieasoup   
<https://blog.csdn.net/CSqingchen/article/details/120156900>  

- 安卓增加 mediasoup webrtc 日志输出  
<https://blog.csdn.net/CSqingchen/article/details/120156669>  

- 安卓 Mediasoup V3 基于webrtc 分支m84 的编译  
<https://blog.csdn.net/CSqingchen/article/details/120163087>

- 安卓 webrtc 开启h264 软编解码  
<https://blog.csdn.net/CSqingchen/article/details/120199702>  

- 安卓mediasoup输出H264流(支持H264编码)  
<https://blog.csdn.net/CSqingchen/article/details/120218832>  

- 安卓mediasoup webrtc h264 软编解码相关源码分析  
<https://blog.csdn.net/CSqingchen/article/details/120218923>  
